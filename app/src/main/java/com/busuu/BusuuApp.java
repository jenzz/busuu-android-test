package com.busuu;

import android.app.Application;
import android.content.Context;
import android.support.v4.app.Fragment;

import dagger.ObjectGraph;

/**
 * Created by jenzz on 22/11/14.
 */
public class BusuuApp extends Application {

	private ObjectGraph objectGraph;

	@Override public void onCreate() {
		super.onCreate();
		buildObjectGraphAndInject();
	}

	private void buildObjectGraphAndInject() {
		objectGraph = ObjectGraph.create(Modules.list(this));
		objectGraph.inject(this);
	}

	public static BusuuApp get(Context context) {
		return (BusuuApp) context.getApplicationContext();
	}

	public static BusuuApp get(Fragment fragment) {
		return get(fragment.getActivity());
	}

	public void inject(Object o) {
		objectGraph.inject(o);
	}
}
