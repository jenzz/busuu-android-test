package com.busuu;

import android.app.Application;
import android.content.res.Resources;
import android.net.Uri;

import com.busuu.data.Database;
import com.busuu.ui.activities.HomeActivity;
import com.busuu.ui.adapters.ReleaseAdapter;
import com.busuu.ui.fragments.ArtistFragment;
import com.busuu.ui.fragments.NavigationDrawerFragment;
import com.squareup.otto.Bus;
import com.squareup.picasso.Downloader;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.Random;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import nl.qbusict.cupboard.CupboardFactory;
import nl.qbusict.cupboard.DatabaseCompartment;

/**
 * Created by jenzz on 22/11/14.
 */
@Module(
		injects = {BusuuApp.class, HomeActivity.class, NavigationDrawerFragment.class, ArtistFragment.class,
				   ReleaseAdapter.class},
		library = true)
public final class BusuuModule {

	private final BusuuApp app;

	public BusuuModule(BusuuApp app) {
		this.app = app;
	}

	@Provides @Singleton Application provideApplication() {
		return app;
	}

	@Provides @Singleton Resources provideResources() {
		return app.getResources();
	}

	@Provides @Singleton Bus provideBus() {
		return new Bus();
	}

	@Provides @Singleton DatabaseCompartment provideDatabase() {
		return CupboardFactory.getInstance().withDatabase(new Database(app).getWritableDatabase());
	}

	@Provides @Singleton Picasso providePicasso() {
		return new Picasso.Builder(app).downloader(new MockDownloader(app)).build();
	}

	private static class MockDownloader implements Downloader {

		private final Application app;

		private MockDownloader(Application app) {
			this.app = app;
		}

		@Override public Response load(Uri uri, boolean localCacheOnly) throws IOException {
			int random = new Random().nextInt(9) + 1; // 1 to 10
			return new Response(app.getAssets().open(random + ".jpg"), true);
		}

		@Override public void shutdown() {
			// no-op
		}
	}
}
