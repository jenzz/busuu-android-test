package com.busuu;

/**
 * Created by jenzz on 22/11/14.
 */
final class Modules {

	private Modules() {
		// no instances
	}

	static Object[] list(BusuuApp app) {
		return new Object[] {new BusuuModule(app)};
	}

}
