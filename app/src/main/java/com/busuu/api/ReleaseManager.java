package com.busuu.api;

import com.busuu.events.ReleasesErrorEvent;
import com.busuu.events.ReleasesLoadedEvent;
import com.discogs.sdk.ArtistsService;
import com.discogs.sdk.DiscogsClient;
import com.discogs.sdk.model.ArtistResponse;
import com.squareup.otto.Bus;

import javax.inject.Inject;
import javax.inject.Singleton;

import nl.qbusict.cupboard.DatabaseCompartment;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by jenzz on 22/11/14.
 */
@Singleton
public class ReleaseManager {

	private final Bus bus;
	private final DatabaseCompartment database;

	@Inject
	public ReleaseManager(Bus bus, DatabaseCompartment database) {
		this.bus = bus;
		this.database = database;
	}

	private final ArtistsService artistsService = DiscogsClient.getInstance().getArtistsService();

	public void getReleases(long artistsId) {
		artistsService.getReleases(artistsId, new Callback<ArtistResponse>() {
			@Override public void success(ArtistResponse artistResponse, Response response) {
				database.put(artistResponse.getReleases());
				bus.post(new ReleasesLoadedEvent(artistResponse.getReleases()));
			}

			@Override public void failure(RetrofitError error) {
				bus.post(new ReleasesErrorEvent(error));
			}
		});
	}

}
