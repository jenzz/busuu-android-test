package com.busuu.data;

import android.content.res.Resources;
import android.util.Pair;

import com.busuu.R;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by jenzz on 22/11/14.
 */
@Singleton
public final class DummyArtists {

	private final Resources res;

	@Inject
	public DummyArtists(Resources res) {
		this.res = res;
	}

	public String[] getArtists() {
		return new String[] {res.getString(R.string.section_deadmau5), res.getString(R.string.section_tiesto),
							 res.getString(R.string.section_calvin_harris)};
	}

	public List<Pair<Long, String>> getArtistsWithId() {
		List<Pair<Long, String>> artists = new ArrayList<>();
		artists.add(Pair.create(257938L, res.getString(R.string.section_deadmau5)));
		artists.add(Pair.create(6197L, res.getString(R.string.section_tiesto)));
		artists.add(Pair.create(222578L, res.getString(R.string.section_calvin_harris)));
		return artists;
	}
}
