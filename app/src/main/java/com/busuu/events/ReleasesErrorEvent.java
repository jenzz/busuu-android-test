package com.busuu.events;

import retrofit.RetrofitError;

/**
 * Created by jenzz on 22/11/14.
 */
public class ReleasesErrorEvent {

	private final RetrofitError error;

	public ReleasesErrorEvent(RetrofitError error) {
		this.error = error;
	}

	public RetrofitError getError() {
		return error;
	}
}
