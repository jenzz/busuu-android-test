package com.busuu.events;

import com.discogs.sdk.model.Release;

import java.util.List;

/**
 * Created by jenzz on 22/11/14.
 */
public class ReleasesLoadedEvent {

	private final List<Release> releases;

	public ReleasesLoadedEvent(List<Release> releases) {
		this.releases = releases;
	}

	public List<Release> getReleases() {
		return releases;
	}
}
