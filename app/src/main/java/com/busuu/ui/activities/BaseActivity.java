package com.busuu.ui.activities;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

import com.busuu.BusuuApp;

/**
 * Created by jenzz on 22/11/14.
 */
public abstract class BaseActivity extends ActionBarActivity {

	@Override protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		BusuuApp.get(this).inject(this);
	}
}
