package com.busuu.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.busuu.BusuuApp;
import com.busuu.R;
import com.discogs.sdk.model.Release;
import com.squareup.otto.Bus;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by jenzz on 22/11/14.
 */
public class ReleaseAdapter extends RecyclerView.Adapter<ReleaseAdapter.ViewHolder> {

	private final List<Release> releases = new ArrayList<>();

	@Inject Bus bus;
	@Inject Picasso picasso;

	public ReleaseAdapter(Context context) {
		BusuuApp.get(context).inject(this);
	}

	public void setReleases(List<Release> releases) {
		this.releases.clear();
		this.releases.addAll(releases);
		notifyDataSetChanged();
	}

	@Override public ViewHolder onCreateViewHolder(ViewGroup viewGroup, final int i) {
		View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.view_artist_release, viewGroup, false);
		return new ViewHolder(view);
	}

	@Override public void onBindViewHolder(ViewHolder viewHolder, int i) {
		Release release = releases.get(i);

		viewHolder.title.setText(release.getTitle());
		viewHolder.year.setText(Integer.toString(release.getYear()));

		String imgUrl = release.getThumbnail();
		if (!TextUtils.isEmpty(imgUrl)) {
			picasso.load(release.getThumbnail()).error(R.drawable.noimageavailable).into(viewHolder.image);
		} else {
			viewHolder.image.setImageResource(R.drawable.noimageavailable);
		}
	}

	@Override public int getItemCount() {
		return releases.size();
	}

	static class ViewHolder extends RecyclerView.ViewHolder {

		@InjectView(R.id.image) ImageView image;
		@InjectView(R.id.title) TextView title;
		@InjectView(R.id.year) TextView year;

		ViewHolder(View itemView) {
			super(itemView);
			ButterKnife.inject(this, itemView);
		}
	}
}
