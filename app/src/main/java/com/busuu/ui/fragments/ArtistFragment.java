package com.busuu.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.busuu.R;
import com.busuu.api.ReleaseManager;
import com.busuu.events.ReleasesErrorEvent;
import com.busuu.events.ReleasesLoadedEvent;
import com.busuu.ui.adapters.ReleaseAdapter;
import com.discogs.sdk.model.Release;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import java.util.List;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import nl.qbusict.cupboard.DatabaseCompartment;

/**
 * Created by jenzz on 22/11/14.
 */
public class ArtistFragment extends BaseFragment {

	private static final String BUNDLE_ARTIST_ID = "artist_id";
	private static final String BUNDLE_ARTIST_NAME = "artist_name";

	public static ArtistFragment newInstance(long artistId, String artistName) {
		ArtistFragment fragment = new ArtistFragment();
		Bundle bundle = new Bundle();
		bundle.putLong(BUNDLE_ARTIST_ID, artistId);
		bundle.putString(BUNDLE_ARTIST_NAME, artistName);
		fragment.setArguments(bundle);
		return fragment;
	}

	@Inject Bus bus;
	@Inject ReleaseManager releaseManager;
	@Inject DatabaseCompartment database;

	@InjectView(R.id.list) RecyclerView list;
	@InjectView(R.id.loading_spinner) View loadingSpinner;

	private ReleaseAdapter adapter;

	@Override public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
									   @Nullable Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_main, container, false);
		ButterKnife.inject(this, view);

		String artistName = getArguments().getString(BUNDLE_ARTIST_NAME);
		getActivity().setTitle(artistName);

		List<Release> cachedReleases = database.query(Release.class).withSelection("artist = ?", artistName).list();
		adapter = new ReleaseAdapter(getActivity());
		adapter.setReleases(cachedReleases);

		int columnCount = getResources().getInteger(R.integer.column_count);
		list.setLayoutManager(new GridLayoutManager(getActivity(), columnCount));
		list.setAdapter(adapter);

		return view;
	}

	@Override public void onResume() {
		super.onResume();
		bus.register(this);

		long artistId = getArguments().getLong(BUNDLE_ARTIST_ID);
		loadReleasesAsync(artistId);
	}

	@Override public void onPause() {
		super.onPause();
		bus.unregister(this);
	}

	@Override public void onDestroyView() {
		super.onDestroyView();
		ButterKnife.reset(this);
	}

	private void loadReleasesAsync(long artistId) {
		if (adapter.getItemCount() == 0) { // show spinner on first load
			loadingSpinner.setVisibility(View.VISIBLE);
		}
		releaseManager.getReleases(artistId);
	}

	@Subscribe
	public void onEvent(ReleasesLoadedEvent e) {
		loadingSpinner.setVisibility(View.GONE);
		adapter.setReleases(e.getReleases());
	}

	@Subscribe
	public void onEvent(ReleasesErrorEvent e) {
		loadingSpinner.setVisibility(View.GONE);
		Toast.makeText(getActivity(), "Oops: " + e.getError().getMessage(), Toast.LENGTH_SHORT).show();
	}
}
