package com.busuu.ui.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.busuu.BusuuApp;

/**
 * Created by jenzz on 22/11/14.
 */
public abstract class BaseFragment extends Fragment {

	@Override public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRetainInstance(true);
		BusuuApp.get(this).inject(this);
	}
}
