package com.discogs.sdk;

import com.discogs.sdk.model.ArtistResponse;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Path;

/**
 * Created by jenzz on 22/11/14.
 */
public interface ArtistsService {

	@GET("/artists/{id}/releases")
	void getReleases(@Path("id") long artistId, Callback<ArtistResponse> callback);

	//
	// MORE ENDPOINTS...
	//

}
