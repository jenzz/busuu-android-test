package com.discogs.sdk;

import com.squareup.okhttp.OkHttpClient;

import java.util.concurrent.TimeUnit;

import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.client.OkClient;

/**
 * Created by jenzz on 22/11/14.
 */
public final class DiscogsClient {

	private static volatile DiscogsClient client;

	private final RestAdapter restAdapter;

	private ArtistsService artistsService;
	private ReleaseService releaseService;

	private DiscogsClient() {
		OkHttpClient httpClient = new OkHttpClient();
		httpClient.setReadTimeout(10, TimeUnit.SECONDS);

		restAdapter = new RestAdapter.Builder().setEndpoint("https://api.discogs.com")
											   .setClient(new OkClient(httpClient))
											   .setRequestInterceptor(new RequestInterceptor() {
												   @Override public void intercept(RequestFacade request) {
													   request.addHeader("User-Agent",
																		 "BusuuAndroidTest/1.0 +http://busuu.com");
												   }
											   })
											   .build();
	}

	// use double-checked locking for thread safety
	public static DiscogsClient getInstance() {
		if (client == null) {
			synchronized (DiscogsClient.class) {
				if (client == null) client = new DiscogsClient();
			}
		}
		return client;
	}

	// lazily init service
	public ArtistsService getArtistsService() {
		if (artistsService == null) {
			artistsService = restAdapter.create(ArtistsService.class);
		}
		return artistsService;
	}

	// lazily init service
	public ReleaseService getReleaseService() {
		if (releaseService == null) {
			releaseService = restAdapter.create(ReleaseService.class);
		}
		return releaseService;
	}

	//
	// MORE SERVICE GETTERS...
	//

}
