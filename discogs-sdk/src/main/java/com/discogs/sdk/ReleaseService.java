package com.discogs.sdk;

import retrofit.http.GET;
import retrofit.http.Path;

/**
 * Created by jenzz on 22/11/14.
 */
public interface ReleaseService {

	@GET("/releases/{id}")
	void getDetails(@Path("id") long releaseId);

	//
	// MORE ENDPOINTS...
	//

}
