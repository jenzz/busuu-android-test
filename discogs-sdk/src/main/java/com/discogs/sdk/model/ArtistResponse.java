package com.discogs.sdk.model;

import java.util.List;

/**
 * Created by jenzz on 22/11/14.
 */
public class ArtistResponse {

	private List<Release> releases;

	public List<Release> getReleases() {
		return releases;
	}
}
