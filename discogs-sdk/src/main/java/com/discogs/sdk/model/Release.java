package com.discogs.sdk.model;

/**
 * Created by jenzz on 22/11/14.
 */
public class Release {

	private int id;
	private String artist;
	private String thumb;
	private String title;
	private int year;

	public int getId() { return id; }

	public String getArtist() { return artist; }

	public String getThumbnail() {
		return thumb;
	}

	public String getTitle() {
		return title;
	}

	public int getYear() {
		return year;
	}
}
